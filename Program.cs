﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionOne
{
    class Program
    {

        // commit1
        // clone2
        static void Main(string[] args)
        {
            List<SmartPhone> smartPhones = new List<SmartPhone>()
            {
                new SmartPhone()
                {
                    Name = "One Plus 9",
                    OS = "Android 11",
                    Price = 55000
                },
                new SmartPhone()
                {
                    Name = "IPhone 11",
                    OS = "IOS 11",
                    Price = 75000
                },
                new SmartPhone()
                {
                    Name = "Samsung Note 9",
                    OS = "Android 9",
                    Price = 110000
                },
                new SmartPhone()
                {
                    Name = "Samsung S20 Ultra",
                    OS = "Android 10",
                    Price = 130000
                },
                new SmartPhone()
                {
                    Name = "IPhone 12 Pro",
                    OS = "IOS 12",
                    Price = 130000
                }
            };
            Console.WriteLine("Sorted based on Price By IComparer");
            Console.WriteLine("=========================");
            SortSmartPhone sorted = new SortSmartPhone();
            smartPhones.Sort(sorted);
            foreach (var item in smartPhones)
            {
                System.Console.WriteLine("Price: " + item.Price + " Name: " + item.Name + " OS: " + item.OS);
            }


            Console.WriteLine("=========================");

            foreach (var item in smartPhones)
            {
                System.Console.WriteLine(smartPhones.Equals(item.OS));
            }


            Console.WriteLine("Sorted based on Name By IComparable");
            Console.WriteLine("=========================");
            smartPhones.Sort();
            foreach (var item in smartPhones)
            {
                System.Console.WriteLine(item.ToString());
            }
            Console.ReadLine();
        }
    }


    // Entity Class
    public class SmartPhone : IComparable , IEquatable<SmartPhone>
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public string OS { get; set; }



        // Comparing Class object (true or false)
        public bool Equals(SmartPhone smartPhone)
        {
            if (Name == smartPhone.Name && Price == smartPhone.Price && OS == smartPhone.OS )
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;
            SmartPhone nextSmartPhone = obj as SmartPhone;
            if (nextSmartPhone != null)
            {
                return this.Name.CompareTo(nextSmartPhone.Name);
            }
            else
            {
                throw new ArgumentException("Object doesn't have a proper Name");
            }
        }

        
        // for printing
        public override string ToString()
        {
            return "Name: " + Name + "||"
                    + " Price: " + Price + "||"
                    + " OS: " + OS ;
        }


    }

    // sorting via IComparer Interface
    public class SortSmartPhone : IComparer<SmartPhone>
    {
        public int Compare(SmartPhone x, SmartPhone y)
        {
            SmartPhone firstDisplay = x as SmartPhone;
            SmartPhone secondDisplay = y as SmartPhone;
            return firstDisplay.Price.CompareTo(secondDisplay.Price);
        }
    }
}
